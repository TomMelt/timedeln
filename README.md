Jakub Benda, 18 July 2019

### Compilation

To compile TIMEDELn with UKRmol+ 2.0 I did the following:

 1. Redefined `int_sp` from a 4-byte integer to the MPI integer. This data type
    is only used for MPI API, which is typically ILP64 when using Intel compilers,
    but can be generally arbitrary (for instance, Archer has LP64).
 2. Copied over the TIMEDELn version of *k_adapt.f90* from UKRmol-out repository.
 3. Wrote CMake build script that manages the compilation, rather than using bunch of
    messy platform-specific Makefiles and shell scripts. All the CMake options are
    documented in the header of *CMakeLists.txt*. Most importantly, the code can now
    be conditionally compiled with or without the adaptive mode (connection to UKRmol+),
    depending on the value of ENABLE_ADAPT (=ON or =OFF) passed to the CMake program,
    and the hard-wired MPI distribution parameters NGEOM, MPI_TASKS_PER_GEOM,
    MPI_TASKS_PER_TD, STARTDIR and STEPDIR became simple CMake options. Ideally, they
    should be command line arguments of TIMEDELn, but that will be the next step.
 4. Cleaned up the file *chdir_c.c* a little: some #include statements were redundant.

The build process results in the library *libtimedeln* and the executable *ptimedel_AxBxC*,
where A = `NGEOM`, B = `MPI_TASKS_PER_GEOM` and C = `MPI_TASKS_PER_TD`.

### Running

Necessary inputs for TIMEDELn:

 1. Channel and target information extracted by SWINTERF (fort.10).
 2. Boundary amplitudes extracted by SWINTERF (fort.21).
 3. Input for TIMEDELn (namelist &time).
 4. Input for RSOLVE on standard input (namelists &rslvin and &bprop).

The inputs 1 to 3 need to be present in directory for each molecular geometry processed concurrently.

The input 4 needs to be passed to the standard input of TIMEDELn program. Watch out, it needs to be
passed to each (!) MPI process, so redirecting it to the "mpiexec" call is not enough (this only
generally connects it to the rank 0). An intermediate wrapper script is needed.

### Parallelization levels

This is how I understand it:

 1. NGEOMS: How many independent calculations (in separate directories) will be run concurrently.
 2. MPI_TASKS_PER_GEOM: How many MPI processes work in each directory.
 3. MPI_TASKS_PER_TD: How many MPI processes work on a single energy range (threshold-to-threshold).
    This needs to be a integer divisor of MPI_TASKS_PER_GEOM.
 4. On the lowest level, TIMEDELn can use OpenMP (via threaded BLAS).
