#!/bin/bash

BUILD="release.cluster.fp64.ilp64.shared"
UKRMOL_IN="/home/physastro/rmatrix/software/intel-19/ukrmol-in-2.0.2/lib"
UKRMOL_OUT="/home/physastro/rmatrix/software/intel-19/ukrmol-out-2.0.1/lib"

# -------------------------------------------------------------------------------------------------------------------------------- #

export PATH=/STEM/software/cmake-3.13.4/bin:$PATH

# -------------------------------------------------------------------------------------------------------------------------------- #

mkdir -p $BUILD
pushd    $BUILD

# -------------------------------------------------------------------------------------------------------------------------------- #

export CC=$(which icc)
export CXX=$(which icpc)
export FC=$(which mpiifort)

export FFLAGS="-i8 -qopenmp -mkl"
export BLA_VENDOR=Intel10_64ilp

cmake \
   -D MINPACK_LIBRARIES="$UKRMOL_OUT/libutil.a" \
   -D UKRMOL_OUT_LIBRARIES="$UKRMOL_OUT/libouter.a;$UKRMOL_IN/libGBTO.a" \
   -D BLA_F95=ON \
   -D NGEOMS=1 \
   -D MPI_TASKS_PER_GEOM=32 \
   -D MPI_TASKS_PER_TD=32 \
   -D STARTDIR=1 \
   -D STEPDIR=1 \
   -D ENABLE_ADAPT=ON \
   ../..

# -------------------------------------------------------------------------------------------------------------------------------- #

make $@

# -------------------------------------------------------------------------------------------------------------------------------- #

popd

# -------------------------------------------------------------------------------------------------------------------------------- #
