#!/bin/bash

. /home/physastro/rmatrix/software/gcc-8/profile.env

BUILD="release.gompi.fp64.ilp64.shared"
UKRMOL_IN="/home/physastro/rmatrix/software/gcc-8/ukrmol-in-2.0.2/lib"
UKRMOL_OUT="/home/physastro/rmatrix/software/gcc-8/ukrmol-out-2.0.1/lib"

# -------------------------------------------------------------------------------------------------------------------------------- #

export PATH=/STEM/software/cmake-3.13.4/bin:$PATH

# -------------------------------------------------------------------------------------------------------------------------------- #

mkdir -p $BUILD
pushd    $BUILD

# -------------------------------------------------------------------------------------------------------------------------------- #

export CC=$(which mpicc)
export CXX=$(which mpicxx)
export FC=$(which mpifort)

export FFLAGS="-fdefault-integer-8 -fopenmp"

cmake \
   -D MINPACK_LIBRARIES="$UKRMOL_OUT/libutil.a" \
   -D UKRMOL_OUT_LIBRARIES="$UKRMOL_OUT/libouter.a;$UKRMOL_IN/libGBTO.a;$UKRMOL_IN/liblapack95.a;$UKRMOL_IN/libblas95.a" \
   -D NGEOMS=1 \
   -D MPI_TASKS_PER_GEOM=32 \
   -D MPI_TASKS_PER_TD=8 \
   -D STARTDIR=1 \
   -D STEPDIR=1 \
   -D ENABLE_ADAPT=ON \
   ../..

# -------------------------------------------------------------------------------------------------------------------------------- #

make $@

# -------------------------------------------------------------------------------------------------------------------------------- #

popd

# -------------------------------------------------------------------------------------------------------------------------------- #
