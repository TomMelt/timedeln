#!/bin/bash

BUILD="serial"
UKRMOL_LIB="/home/melt/software/UKRmol-in/master_doub/lib/"

# -------------------------------------------------------------------------------------------------------------------------------- #

rm -rf $BUILD
mkdir -p $BUILD
cd       $BUILD

# -------------------------------------------------------------------------------------------------------------------------------- #

export CC=$(which icc)
export CXX=$(which icpc)
export FC=$(which mpiifort)

export FFLAGS="-i8 -qopenmp -mkl -O0"
export BLA_VENDOR=Intel10_64ilp

cmake \
   -D MINPACK_LIBRARIES="$UKRMOL_LIB/libutil.a" \
   -D UKRMOL_OUT_LIBRARIES="$UKRMOL_LIB/libouter.a;$UKRMOL_LIB/libGBTO.a" \
   -D BLA_F95=ON \
   -D NGEOMS=1 \
   -D MPI_TASKS_PER_GEOM=1 \
   -D MPI_TASKS_PER_TD=1 \
   -D STARTDIR=1 \
   -D STEPDIR=1 \
   -D ENABLE_ADAPT=ON \
   -D CMAKE_BUILD_TYPE=Release \
   ../..

#   -D CMAKE_BUILD_TYPE=Debug \
# -------------------------------------------------------------------------------------------------------------------------------- #

make $@

cd ..

# -------------------------------------------------------------------------------------------------------------------------------- #
