module ftn_c
  interface
    subroutine chdir_c(istartdir, istepdir, my_id) bind(C)
      use iso_c_binding, only: C_INT
      integer (kind=c_int), VALUE :: istartdir
      integer (kind=c_int), VALUE :: istepdir
      integer (kind=c_int), VALUE :: my_id
    end subroutine chdir_c
  end interface
end module ftn_c
