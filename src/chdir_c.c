#include <stdio.h>

void chdir_c(int istartdir, int istepdir, int igroup)
{

   int check, check1;
   int idirname, ierr;
   char dirname[40];

   extern void outer_();

   printf("Hello world! I'm in group %i\n", igroup);

  idirname = istartdir + (igroup*istepdir);

  snprintf(dirname, 40, "%d", idirname);

  printf ( "Process %i Changing Directory to %s\n", igroup, dirname);

  check1 = chdir(dirname);
  if(check1!=0){
       perror("Error  with chdir:\n"); }
}

