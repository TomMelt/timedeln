module comm_mpi
! Routines providing MPI communication between processors
! Time-stamp: "2012-02-05 00:35:59 cjn"
! Adapted AGS Jul 2015 to include new MPI communicators (restricted to 1 level of splitting)

  use mpi  ! Modules only support 32-bit integers

  use io_units, only: fo
  use precisn, only: wp
  implicit none
!  include 'mpif.h'
! define MPI communicator:
  integer, save            :: LOCAL_MPI_COMM = MPI_COMM_WORLD
! define new MPI communicator
!  integer, save            :: new_comm   ! New MPI Comm
  integer, save            :: myrank     ! processor rank
  integer, save            :: com_size   ! # processors in communicator
  integer, save            :: rtype      ! MPI wp type

  integer, save            :: barrier = 0
  integer, save            :: gbarrier = 0

  interface distribute_array
     module procedure distribute_iarray
     module procedure distribute_rarray
     module procedure distribute_r2array
     module procedure distribute_i2array
     module procedure distribute_carray
     module procedure distribute_larray
  end interface

  interface get_stats
     module procedure get_int_stats
     module procedure get_real_stats
  end interface

  interface sum_procs
     module procedure isum_procs
     module procedure rsum_procs
  end interface

  private
  public merged_index, initialize_mpi, stop_mpi, myrank, com_size
  public distribute_array, distribute_integer, finalize_mpi
  public get_stats, merge_real_array
  public set_barrier, set_global_barrier, LOCAL_MPI_COMM
  public get_mpi_int_array, get_mpi_real_array
  public rtype, sum_procs
  public split_comm, set_new_comm, reset_to_global_comm, print_comm_info
contains

  subroutine initialize_mpi
    logical                            :: flag
    integer                            :: err
    character(len=MPI_MAX_OBJECT_NAME) :: comm_name
    character(len=MPI_MAX_OBJECT_NAME) :: comm_id
    integer                            :: len_comm_name

! Initialize MPI:
    call MPI_INITIALIZED (flag, err)
    if (.NOT.flag) then
       call MPI_INIT (err)
       if (err /= 0) then
          write (fo,'(a,i4)') 'initialize_mpi: start error =', err
          call stop_mpi
       end if
    end if
    LOCAL_MPI_COMM = MPI_COMM_WORLD
    call MPI_COMM_RANK (LOCAL_MPI_COMM, myrank, err)     ! rank
    call MPI_COMM_SIZE (LOCAL_MPI_COMM, com_size, err)   ! COM size
    comm_id = 'INITIAL_GLOBAL_MPI_COMM'
    call MPI_COMM_SET_NAME (LOCAL_MPI_COMM, TRIM(comm_id), err ) 
    call MPI_COMM_GET_NAME (LOCAL_MPI_COMM, comm_name, len_comm_name, &
         err)
    if (myrank == 0) then
       write (fo,'(a,i6)') 'MPI initialized: number of processors = ',&
            com_size
       write (fo,'(a)') 'Communicator name = ' // TRIM(comm_name)
    end if
    RTYPE = MPI_DOUBLE_PRECISION
  end subroutine initialize_mpi

  subroutine set_barrier(comm)
    integer, intent(in), optional :: comm  ! MPI communicator
    integer                       :: err, commun

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
  
    barrier = barrier + 1
    call MPI_BARRIER (commun, err)
    if (err /= 0) then
       write (fo, *) 'set_barrier: MPI_BARRIER error = ', err
       call stop_mpi
    end if
  end subroutine set_barrier

  subroutine set_global_barrier
    integer           :: err

    gbarrier = gbarrier + 1
    call MPI_BARRIER (MPI_COMM_WORLD, err)
    if (err /= 0) then
       write (fo, *) 'set_global_barrier: MPI_BARRIER error = ', err
       call stop_mpi
    end if
  end subroutine set_global_barrier

  function rand_int(a, b)
    integer             :: rand_int
    integer, intent(in) :: a, b
    real(wp)            :: x
    call random_number (x)
    rand_int = NINT(a + (b - a) * x)
  end function rand_int

  subroutine finalize_mpi
! end MPI processing
    integer                :: err
    call MPI_FINALIZE (err)
    if (err /= 0) call stop_mpi
    if (myrank == 0) write (fo,'(a)') 'MPI shutdown.'
  end subroutine finalize_mpi

  subroutine merged_index (ic)
! send data to node 0 for output to index record
    integer, intent(inout)  :: ic(:)   ! merged list
    integer                 :: err, err1, i, nic, tag
    integer, allocatable    :: ibuf(:)
    integer                  :: status(MPI_STATUS_SIZE)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)
    nic = SIZE(ic)
    if (myrank == 0) then
       allocate (ibuf(nic), stat=err1)
       if (err1 /= 0) then
          write (fo,'(a,i6)') 'merged_index: allocation error', err1
          call stop_mpi
       end if
    end if
    processors: do i = 1, com_size-1
       tag = i
       if (myrank == i) then
          call MPI_SEND (ic, nic, MPI_INTEGER, 0, tag, LOCAL_MPI_COMM, &
               err)
          if (err /= 0) call stop_mpi
       else if (myrank == 0) then
          ibuf = 0
          call MPI_RECV (ibuf, nic, MPI_INTEGER, MPI_ANY_SOURCE, tag, &
               LOCAL_MPI_COMM, status, err)
          if (err /= 0) call stop_mpi
          ic = ic + ibuf
       end if
    end do processors
    if (myrank == 0) then
       deallocate (ibuf, stat=err1)
       if (err1 /= 0) then
          write (fo,'(a,i6)') 'merged_index: deallocation error', err1
          call stop_mpi
       end if
    end if
  end subroutine merged_index

  subroutine distribute_integer (n, comm, proc)
! Distribute an integer to all nodes
    integer, intent(inout)        :: n     ! integer to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                       :: err, commun, p, mycom_rank
    integer                       :: ia(1)

    ia(1) = n
    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if
    call MPI_COMM_RANK (commun, mycom_rank, err)     ! rank
    call MPI_BCAST (ia, 1, MPI_INTEGER, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_integer: MPI_BCAST error = ', err
       call stop_mpi
    end if
    n = ia(1)
  end subroutine distribute_integer
 
  subroutine distribute_carray (nv, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)             :: nv    ! size of array
    character(LEN=1), intent(inout) :: v(:)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                         :: err, commun, p

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if
    call MPI_BCAST (v, nv, MPI_CHARACTER, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_carray: MPI_BCAST error = ', err
       call stop_mpi
    end if
  end subroutine distribute_carray

  subroutine distribute_iarray (nv, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)     :: nv    ! size of array
    integer, intent(inout)  :: v(:)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                         :: err, commun, p

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if
    call MPI_BCAST (v, nv, MPI_INTEGER, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_carray: MPI_BCAST error = ', err
       call stop_mpi
    end if
  end subroutine distribute_iarray

  subroutine distribute_larray (nv, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)     :: nv    ! size of array
    logical, intent(inout)  :: v(:)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                         :: err, commun, p

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if
    call MPI_BCAST (v, nv, MPI_LOGICAL, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_larray: MPI_BCAST error = ', err
       call stop_mpi
    end if
  end subroutine distribute_larray

  subroutine distribute_rarray (nv, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)     :: nv    ! size of array
    real(wp), intent(inout) :: v(:)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                       :: err, commun, p

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if
    call MPI_BCAST (v, nv, rtype, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_rarray: MPI_BCAST error = ', err
       call stop_mpi
    end if
  end subroutine distribute_rarray

  subroutine distribute_r2array (nv, nw, nvd, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)     :: nv    ! size of array (dim 1)
    integer, intent(in)     :: nw    ! size of array (dim 2)
    integer, intent(in)     :: nvd    ! dimension of 1st array
    real(wp), intent(inout) :: v(nvd, nw)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                       :: err, commun, p, nvw, j, i
    real(wp)                      :: tmp(nv*nw)

    j = 0
    do i = 1, nw
       tmp(j+1:j+nv) = v(1:nv,i)
       j = j + nv
    end do
    nvw = nv * nw

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if

    call MPI_BCAST (tmp, nvw, rtype, p, commun, err)
    if (err /= 0) then
       write (fo,*) 'distribute_rarray: MPI_BCAST error = ', err
       call stop_mpi
    end if

    j = 0
    do i = 1, nw
       v(1:nv,i) = tmp(j+1:j+nv)
       j = j + nv
    end do
  end subroutine distribute_r2array

  subroutine distribute_i2array (nv, nw, v, comm, proc)
! Distribute an array to all nodes
    integer, intent(in)     :: nv    ! size of array
    integer, intent(in)     :: nw    ! size of array
    integer, intent(inout)  :: v(nv,nw)  ! array to be distributed
    integer, intent(in), optional :: comm  ! MPI communicator
    integer, intent(in), optional :: proc  ! originating MPI processor
    integer                 :: err, i, j
    integer                 :: tmp(nv*nw)
    integer                 :: commun, p

    j = 0
    do i = 1, nw
       tmp(j+1:j+nv) = v(1:nv,i)
       j = j + nv
    end do

    if (PRESENT(comm)) then
       commun = comm
    else
       commun = LOCAL_MPI_COMM
    end if
    if (PRESENT(proc)) then
       p = proc
    else
       p = 0
    end if

    call MPI_BCAST (tmp, nv*nw, MPI_INTEGER, p, commun, err)
    if (err /= 0) call stop_mpi
    j = 0
    do i = 1, nw
       v(1:nv,i) = tmp(j+1:j+nv)
       j = j + nv
    end do
  end subroutine distribute_i2array

  subroutine stop_mpi
    integer    :: err, err1
    call MPI_ABORT (LOCAL_MPI_COMM, err, err1)
    if (err1 /= 0) then
       write (fo,*) myrank, ' stop_mpi: MPI_ABORT error', err1
    end if
  end subroutine stop_mpi

  subroutine get_int_stats (val)
! sum statistical information on node 0
    integer, intent(inout)  :: val       ! values to sum
    integer                 :: err, b1(1), b2(1)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)
    b1(1) = val
    b2(1) = 0
    call MPI_REDUCE (b1, b2, 1, MPI_INTEGER, MPI_SUM, 0, &
         LOCAL_MPI_COMM, err)
    val = b2(1)
  end subroutine get_int_stats

   subroutine get_real_stats (val)
! sum statistical information on node 0
    real(wp), intent(inout)  :: val       ! values to sum
    integer                  :: err
    real(wp)                 :: b1(1), b2(1)
!    real         :: b1(1), b2(1)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)
    b1(1) = val
    b2(1) = 0.0_wp
    call MPI_REDUCE (b1, b2, 1, MPI_REAL, MPI_SUM, 0, &
         LOCAL_MPI_COMM, err)
! Add filehand and index records:
    if (myrank == 0) val = b2(1)
  end subroutine get_real_stats

  subroutine get_mpi_int_array (iarray, itable)
    integer, intent(in)       :: iarray(:) ! input array
    integer, pointer          :: itable(:,:)
    integer                   :: i, err, tag
    integer                   :: status(MPI_STATUS_SIZE)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)

    if (myrank == 0) then
       allocate (itable(SIZE(iarray),0:com_size-1), stat=err)
       if (err /= 0) then
          write (fo,'(a,i6)') 'get_mpi_int_array: allocation error = ',&
               err
          call stop_mpi
       end if
       itable = 0.0_wp
       itable(:,0) = iarray
    end if
    processors: do i = 1, com_size-1
       tag = i
       if (myrank == i) then
          call MPI_SEND (iarray, SIZE(iarray), MPI_INTEGER, 0, tag, &
               LOCAL_MPI_COMM, err)
          if (err /= 0) then
             write (fo,'(a,i6)') 'get_mpi_int_array: send error = ', err
             call stop_mpi
          end if
       else if (myrank == 0) then
          call MPI_RECV (itable(:,i), SIZE(iarray), MPI_INTEGER, i,&
               tag, LOCAL_MPI_COMM, status, err)
          if (err /= 0) then
             write (fo,'(a,i6)') 'get_mpi_int_array: recv error = ', err
             call stop_mpi
          end if
       end if
    end do processors
  end subroutine get_mpi_int_array

  subroutine get_mpi_real_array (rarray, rtable)
! accumulate array from each processor node on node 0
    real(wp), intent(in)      :: rarray(:) ! input array
    real(wp), pointer         :: rtable(:,:) ! output table
    integer                   :: i, err, tag
    integer                   :: status(MPI_STATUS_SIZE)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)
    if (myrank == 0) then
       allocate (rtable(SIZE(rarray),0:com_size-1), stat=err)
       if (err /= 0) then
          write (fo,'(a,i6)') 'get_mpi_real_array: allocation &
               &error = ', err
          call stop_mpi
       end if
       rtable = 0.0_wp
       rtable(:,0) = rarray
    end if
    processors: do i = 1, com_size-1
       tag = i
       if (myrank == i) then
          call MPI_SEND (rarray, SIZE(rarray), rtype, &
               0, tag, LOCAL_MPI_COMM, err)
          if (err /= 0) then
             write (fo,'(a,i6)') 'get_mpi_real_array: send error = ', err
             call stop_mpi
          end if
       else if (myrank == 0) then
          call MPI_RECV (rtable(:,i), SIZE(rarray), rtype, i,&
               tag, LOCAL_MPI_COMM, status, err)
          if (err /= 0) then
             write (fo,'(a,i6)') 'get_mpi_real_array: recv error = ', err
             call stop_mpi
          end if
       end if
    end do processors
  end subroutine get_mpi_real_array

  subroutine merge_real_array (c)
! send data to node 0 for output to merged array
    real(wp), intent(inout)  :: c(:)   ! merged array
    integer                  :: err, err1, i, nc, tag
    real(wp), allocatable    :: buf(:)
    integer                  :: status(MPI_STATUS_SIZE)

    call MPI_BARRIER (LOCAL_MPI_COMM, err)
    nc = SIZE(c)
    if (myrank == 0) then
       allocate (buf(nc), stat=err1)
       if (err1 /= 0) then
          write (fo,'(a,i6)') 'merge_real_array: allocation error',&
               err1
          call stop_mpi
       end if
    end if
    processors: do i = 1, com_size-1
       if (myrank == i) then
          tag = i
          call MPI_SEND (c, nc, rtype, 0, tag, LOCAL_MPI_COMM, err)
          if (err /= 0) call stop_mpi
       else if (myrank == 0) then
          buf = 0
          call MPI_RECV (buf, nc, rtype, MPI_ANY_SOURCE, tag, &
               LOCAL_MPI_COMM, status, err)
          if (err /= 0) call stop_mpi
          c = c + buf
       end if
    end do processors
    if (myrank == 0) then
       deallocate (buf, stat=err1)
       if (err1 /= 0) then
          write (fo,'(a,i6)') 'merge_real_array: deallocation error', err1
          call stop_mpi
       end if
    end if
  end subroutine merge_real_array
  
  subroutine isum_procs (sendbuf, recvbuf)
    integer, intent(in)               :: sendbuf(:)
    integer, intent(out), allocatable :: recvbuf(:)
    integer                           :: count, err

    count = SIZE(sendbuf)
    allocate (recvbuf(count), stat=err)

    call MPI_reduce (sendbuf, recvbuf, count, MPI_INTEGER, MPI_SUM, &
         0, LOCAL_MPI_COMM, err) 
  end subroutine isum_procs

  subroutine rsum_procs (sendbuf, recvbuf)
    real(wp), intent(in)               :: sendbuf(:)
    real(wp), intent(out), allocatable :: recvbuf(:)
    integer                            :: count, err

    count = SIZE(sendbuf)
    allocate (recvbuf(count), stat=err)

    call MPI_reduce (sendbuf, recvbuf, count, rtype, MPI_SUM, &
         0, LOCAL_MPI_COMM, err) 
  end subroutine rsum_procs

  subroutine set_new_comm(specified_comm)
    integer, intent(in)                :: specified_comm   ! Specified MPI Comm
    logical                            :: flag
    integer                            :: err
    character(len=MPI_MAX_OBJECT_NAME) :: comm_name
    integer                            :: len_comm_name

! Check MPI is initialized:
    call MPI_INITIALIZED (flag, err)
    if (.NOT.flag) then
          write (fo,'(a,i4)') 'set_comm: MPI not initialized'
          call stop_mpi
    end if

    LOCAL_MPI_COMM = specified_comm

    call MPI_COMM_RANK (LOCAL_MPI_COMM, myrank, err)     ! rank
    if (err /= 0) then
       write (fo,'(a,i4)') 'set_comm: MPI_COMM_RANK error =', err
       call stop_mpi
    end if

    call MPI_COMM_SIZE (LOCAL_MPI_COMM, com_size, err)   ! COM size
    if (err /= 0) then
       write (fo,'(a,i4)') 'set_new_comm: MPI_COMM_SIZE error =', err
       call stop_mpi
    end if

    call MPI_COMM_GET_NAME (LOCAL_MPI_COMM, comm_name, len_comm_name, &
         err)
    if (err /= 0) then
       write (fo,'(a,i4)') 'set_new_comm: MPI_COMM_GET_NAME error =', err
       call stop_mpi
    end if

    if (myrank == 0) then
       write (fo,'(a,i6)') 'set_new_comm: number of processors = ',&
            com_size
       write (fo,'(a)') 'Communicator name = ' // TRIM(comm_name)
    end if
  end subroutine set_new_comm

  subroutine reset_to_global_comm
    logical                            :: flag
    integer                            :: err
    character(len=MPI_MAX_OBJECT_NAME) :: comm_name
    integer                            :: len_comm_name

    ! Check MPI is initialized:
    call MPI_INITIALIZED (flag, err)
    if (.NOT.flag) then
       write (fo,'(a,i4)') 'set_comm: MPI not initialized'
       call stop_mpi
    end if

    LOCAL_MPI_COMM = MPI_COMM_WORLD
    call MPI_COMM_RANK (LOCAL_MPI_COMM, myrank, err)     ! rank
    if (err /= 0) then
       write (fo,'(a,i4)') 'reset_to_global_comm: MPI_COMM_RANK error =', err
       call stop_mpi
    end if

    call MPI_COMM_SIZE (LOCAL_MPI_COMM, com_size, err)   ! COM size
    if (err /= 0) then
       write (fo,'(a,i4)') 'reset_to_global_comm: MPI_COMM_SIZE error =', err
       call stop_mpi
    end if

    call MPI_COMM_GET_NAME (LOCAL_MPI_COMM, comm_name, len_comm_name, &
         err)
    if (err /= 0) then
       write (fo,'(a,i4)') 'reset_to_global_comm: MPI_COMM_GET_NAME error =', err
       call stop_mpi
    end if

    if (myrank == 0) then
       write (fo,'(a,i6)') 'reset_to_global_comm: number of processors = ',&
            com_size
       write (fo,'(a)') 'Communicator name = ' // TRIM(comm_name)
    end if
    RTYPE = MPI_DOUBLE_PRECISION
  end subroutine reset_to_global_comm

  subroutine split_comm(colour,key,new_comm, parent_comm, global)
    integer, intent(in)       :: colour
    integer, intent(in)       :: key
    integer, intent(out)      :: new_comm
    integer, optional, intent(in) :: parent_comm
    logical, optional, intent(in) :: global
    logical                   :: flag, global_world
    integer                   :: err
    character(len=MPI_MAX_OBJECT_NAME) :: comm_name
    character(len=MPI_MAX_OBJECT_NAME) :: comm_id
    integer                   :: len_comm_name, local_parent_comm
    character(13)             :: stem='MPI_SUBGROUP_'
    character(2)              :: ch_clr
    character(1)              :: tmp
    character(1)              :: ch_zero = '0'
 
    ! check global
! global is optional, used to set either LOCAL_MPI_COMM or MPI_COMM_WORLD 
! as the parent if parent_comm is absent
! default is .FALSE. ie LOCAL_MPI_COMM is the parent
    if (PRESENT(global)) then
       global_world = global
    else
       global_world = .false.
    end if 
    if (PRESENT(parent_comm)) then
       local_parent_comm = parent_comm
    else
       if (global_world) then
          local_parent_comm = MPI_COMM_WORLD
       else
          local_parent_comm = LOCAL_MPI_COMM
       end if
    end if

    ! Check MPI is initialized:
    call MPI_INITIALIZED (flag, err)
    if (.NOT.flag) then
       write (fo,'(a)') 'split_comm: MPI not initialized'
       call stop_mpi
    end if


    
    CALL MPI_COMM_SPLIT(local_parent_comm,colour,key,new_comm,err)
    LOCAL_MPI_COMM=new_comm

    call MPI_COMM_RANK (LOCAL_MPI_COMM, myrank, err)     ! rank
    if (err /= 0) then
       write (fo,'(a,i4)') 'split_comm: MPI_COMM_RANK error =', err
       call stop_mpi
    end if

    call MPI_COMM_SIZE (LOCAL_MPI_COMM, com_size, err)   ! COM size
    if (err /= 0) then
       write (fo,'(a,i4)') 'split_comm: MPI_COMM_SIZE error =', err
       call stop_mpi
    end if
    
    if (colour<10) then
      write(tmp,"(i1)") colour
      write(ch_clr,"(a1,a1)") ch_zero,tmp
    else if ((colour >=10).and.(colour<100)) then
      write(ch_clr,"(i2)") colour
    else
      write (fo,'(a)') 'split_comm: Number of MPI GROUPS > MAX allowable'
      call stop_mpi
    end if

    write(comm_id,"(a13,a2)" ) stem,TRIM(adjustl(ch_clr))
    call MPI_COMM_SET_NAME (LOCAL_MPI_COMM, TRIM(comm_id), err)
    call MPI_COMM_GET_NAME (LOCAL_MPI_COMM, comm_name, len_comm_name, &
         err)
!    write (fo,'(a)') 'Communicator name = ' // TRIM(comm_name)
    if (err /= 0) then
       write (fo,'(a,i4)') 'split_comm: MPI_COMM_GET_NAME error =', err
       call stop_mpi
    end if

 !   if (myrank == 0) then
 !      write (fo,'(a,i6)') 'split_comm: number of processors = ',&
 !           com_size
 !      write (fo,'(a)') 'Communicator name = ' // TRIM(comm_name)
 !   end if
 !   RTYPE = MPI_DOUBLE_PRECISION
  end subroutine split_comm

  subroutine print_comm_info

    ! Print MPI Comm information (mainly for debugging purposes):
    logical                   :: flag
    integer                   :: err
    character(len=MPI_MAX_OBJECT_NAME) :: comm_name
    character(len=MPI_MAX_OBJECT_NAME) :: comm_id
    integer                   :: len_comm_name
    character(len=13)         :: stem='MPI_SUBGROUP_'
    character(len=12)         :: str_myrank, str_gmyrank
    character(len=12)         :: str_size, str_gsize
    character(len=2)          :: ch_clr
    character(len=1)          :: tmp
    character(len=1)          :: ch_zero = '0'
    integer                   :: gmyrank, gcom_size
    
 
    ! Check MPI is initialized:
    call MPI_INITIALIZED (flag, err)
    if (.NOT.flag) then
       write (fo,'(a,i4)') 'print_comm_info: MPI not initialized'
       return
    end if

    call MPI_COMM_RANK (LOCAL_MPI_COMM, myrank, err)     ! rank
    if (err /= 0) then
       write (fo,'(a,i4)') 'print_comm_info: MPI_COMM_RANK error =', err
       call stop_mpi
    end if

  call MPI_COMM_RANK (MPI_COMM_WORLD, gmyrank, err)     ! rank
    if (err /= 0) then
       write (fo,'(a,i4)') 'print_comm_info: MPI_COMM_RANK error =', err
       call stop_mpi
    end if

    call MPI_COMM_SIZE (MPI_COMM_WORLD, gcom_size, err)   ! com size
    if (err /= 0) then
       write (fo,'(a,i4)') 'print_comm_info: MPI_COMM_SIZE error =', err
       call stop_mpi
    end if

    call MPI_COMM_GET_NAME (LOCAL_MPI_COMM, comm_name, len_comm_name, &
         err)

    ! Internal writes for neat output   
    write (str_myrank,'(i8)') myrank
    write (str_gmyrank,'(i8)') gmyrank
    write (str_size,'(i8)') com_size
    write (str_gsize,'(i8)') gcom_size

    call set_barrier

    if(myrank.eq.0) write (fo,'(3a)') 'Comm Name = ' &
      // TRIM(comm_name), ' Comm Size = ', trim(adjustl(str_size)), &
      ' ( '// trim(adjustl(str_gsize)) //' ) '

    call set_barrier
!    if(gmyrank==0) write (fo,'(\)')    
    if(gmyrank==0) write (fo,*)    
    call set_barrier

    write (fo,'(3a)') 'Comm Name = ' // trim(comm_name), &
      ' , Comm Rank (Global Rank) = '// trim(adjustl(str_myrank)), &
      ' ( '// trim(adjustl(str_gmyrank)) // ' ) ' 
    call set_barrier

!    if(myrank==0) write (fo,'(\)')
    if(myrank==0) write (fo,*)

    call set_global_barrier

    if (err /= 0) then
       write (fo,'(a,i4)') 'print_comm_info: MPI_COMM_GET_NAME error =', err
       call stop_mpi
    end if

  end subroutine print_comm_info
  
end module comm_mpi
