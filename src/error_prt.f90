module error_prt
! standard reporting of error conditions
! Time-stamp: "2011-11-15 20:26:05 cjn62"
  use io_units, only: fo
  use comm_mpi, only: stop_mpi, myrank
  implicit none

  private
  public eprt, emsg, ecode, meprt
!  public perr, allocerr, ioerr, ecode

  character(LEN=200) :: emsg
  integer            :: ecode

  interface eprt
     module procedure perr, allocerr, ioerr
  end interface eprt
  

contains

  subroutine perr (caller, msg1, ecode1)
! general error condition halt
    character(LEN=*), intent(in)      :: caller
    character(LEN=*), intent(in)      :: msg1
    integer, optional, intent(in)     :: ecode1

    write (fo,'(a,i6)') 'Abort called, rank = ', myrank
    if (PRESENT(ecode1)) then
       write (fo,'(a,i6)') TRIM(caller) // ': ' // TRIM(msg1), ecode1
    else 
       write (fo,'(a)') TRIM(caller) // ': ' // TRIM(msg1)
    end if
    call stop_mpi
  end subroutine perr

  subroutine allocerr (caller)
! allocate/deallocate error message
    character(LEN=*), intent(in)      :: caller

    write (fo,'(a,i6)') 'Abort called, rank = ', myrank
    write (fo,'(a)') 'allocation/deallocation failure'
    write (fo,'(a)') 'caller: ' // TRIM(caller)
    write (fo,'(a,i6)') TRIM(emsg), ecode
    call stop_mpi
  end subroutine allocerr

  subroutine ioerr (caller, un)
! i/o error message
    character(LEN=*), intent(in)      :: caller
    integer, intent(in)               :: un

    write (fo,'(a,i6)') 'Abort called, rank = ', myrank
    write (fo,'(a,i3)') 'i/o operation failure: unit = ', un
    if (is_iostat_eor(ecode)) write (fo,'(a)') 'end of record error'
    if (is_iostat_end(ecode)) write (fo,'(a)') 'end of file error'
    write (fo,'(a)') 'caller: ' // TRIM(caller)

    write (fo,'(a,i6)') TRIM(emsg), ecode
    call stop_mpi
  end subroutine ioerr

  subroutine meprt (caller)
! MPI function error condition halt
    character(LEN=*), intent(in)      :: caller
    integer                           :: msglen, err

    call MPI_ERROR_STRING (ecode, emsg, msglen, err)
    write (fo,'(i6,1x,a)') myrank, TRIM(caller) // ': ' // emsg(1:msglen)
    call stop_mpi
  end subroutine meprt
end module error_prt
