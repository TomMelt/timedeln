! Copyright 2019
!
! For a comprehensive list of the developers that contributed to these codes
! see the UK-AMOR website.
!
! This file is part of UKRmol-out (UKRmol+ suite).
!
!     UKRmol-out is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     UKRmol-out is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with  UKRmol-out (in source/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!
C**********************************************************************
! This version of k.adapt.f is to be used with the parallel version of
! timedelay, timedelN (see 10.1016/j.cpc.2017.01.005). 
! This program can be obtained from the CPC program library:
!  https://data.mendeley.com/datasets/wmv4f42xnz/2
! Details of how to compile and run timedelN, with test cases, can be
! found there.
C*********************************************************************

      module userdefkmat
!      implicit double precision(a-h,o-z)
      implicit none
! The variables used in the USER-defined K-matrix routines should 
! be defined in this module
C
C***********************************************************************
C
      integer :: maxpts
      PARAMETER (MAXPTS=30) !    MAXPTS = MAXIMUM NUMBER OF GEOMETRIES
      CHARACTER*80 NAME
      INTEGER STOT,GUTOT
      integer :: IPRNT(6),iback
      EXTERNAL POTL,DISPOT
      integer :: knots,nvchan,nvibd,nquad
      integer :: nocsf,nfbut,nesc,newbut,ntargv,ion
      integer :: ntarg,nchanf,nhd,nchan,nstat
      integer :: nlpole,mdmax,ismax,ngeom
      integer :: mgvn,iwrite,ndis,ivprop
      integer :: idprop,nvib,npole,nthreshols
      double precision :: emaxm,e2m,enryd,etot,e0,ryd
      double precision :: ebase,eminm,rafinv,scale,rafind,emaxr
      double precision :: rmass,rmatr,twom,eminr,emin,emax
      double precision :: RK(MAXPTS+4),R(MAXPTS), ezero(maxpts)
      integer, allocatable :: ichord(:),ivtarg(:),ivnu(:),starg(:),
     * gtarg(:),mtarg(:),ivchl(:),lvchl(:),mvchl(:),ichl(:),ncsf(:)
      double precision, allocatable :: ampn(:),eign(:),cf(:),
     * evib(:),etarg(:),amc(:),adc(:), sfac(:), ecex(:),rcex(:),
     * vibfn(:),rquad(:),qwts(:),evchl(:),epole(:),wamp(:),butc(:),
     * adm(:),bloch(:)
!
      CHARACTER*3 EUNIT(2)
      double precision :: toev,pi
      parameter (pi=3.14159265d0)
c 
      
      DATA IWRITE/6/,IPRNT/6*0/,IVPROP/1/,IDPROP/1/,NPOLE/0/,
     * NVIB/0/,NDIS/0/,ISMAX/-1/,NGEOM/1/,NLPOLE/1/,newbut/1/,MDMAX/-1/
      DATA EUNIT/'RYD','EV'/,RYD/0.073500D0/
      DATA IBACK/1/
      end module userdefkmat
!
      subroutine setupkmat(einit,efinal,maxthresh,etarget,ntarget,iam)
      use userdefkmat
      implicit double precision (a-h,o-z)
c
C     MAXIMUM DIMENSIONS ARE SET BY THE FOLLOWING PARAMETER STATEMENT
C     VARIABLE DIMENSIONS ARE USED IN ALL LOWER LEVEL ROUTINES, EXCEPT
C     VIBINI 
C
      PARAMETER (MAXTGT=50,MAXENR=10)
C
      double precision :: einit,efinal,etarget(maxthresh)
      integer :: ivt0(2),ivu0(2),maxthresh,ntarget,NESCAT(MAXENR),iam
      integer :: NCHSET(MAXPTS),NRMSET(MAXPTS),nvtarg(maxtgt)
      logical :: qmoln
      CHARACTER*11 RFORM,CHFORM,WFORM,KFORM,NRFORM,VCFORM,MODDAT
      CHARACTER*1 IRFORM,ICFORM,IWFORM,IKFORM,INRFRM,IVCFRM

      CHARACTER*8 BLANK
      CHARACTER*9 FORM
      CHARACTER*20 DAYTIM
      double precision :: EINR(2,MAXENR),EINC(2,MAXENR)
      integer :: isf_d, irc_d iex_d 
c
C
C***********************************************************************
C
C     BASIC DATA IS INPUT VIA NAMELIST /RSLVIN/
C     OTHER DATA IS INPUT VIA NAMELISTS IN ROUTINES VIBINI AND ASYM1
C
C      BBLOCH   = COEFFICIENT IN ELECTRONIC BLOCH OPERATOR
C      BIGB     = COEFFICIENT IN NUCLEAR BLOCH OPERATOR
C      EINC     = Scattering energies relative to lowest (vibrational)
C                 level of target 
C                 EINC(1,I) = initial energy in sub range I
C                 EINC(2,I) = energy increment in this subrange
C                 units are as specified by IEUNIT
C      GUTOT    = G/U SYMMETRY OF TOTAL SYSTEM +1=G, -1=U
C      ICFORM   = Formatted/unformatted switch for unit LUCHAN
C      IEUNIT   = UNITS IN WHICH INPUT SCATTERING ENERGIES ARE INPUT
C                 1= RYD, 2= EV
C      IKFORM   = Formatted/unformatted switch for unit LUKMT
C      INRFRM   = Formatted/unformatted switch for unit LUNRMT
C      IPRNT    = DEBUG PRINT SWITCHES
C                 (1) =1 Print all input data
C                 (2) =1 Print vibrational wavefuction data
C                 (3) =1 Debug output in dissociating channels
C                 (4)  not used
C                 (5) =1 Print R-matrices
C                 (6) =1 Print all output data
C      IRFORM   = Formatted/unformatted switch for unit LURMT
C      ISMAX    = Highest multipole to be used in asymptotic expansion
C                 of asymptotic potentials
C      IWFORM   = Formatted/unformatted switch for unit LUWFN
C      IWRITE   = Logical unit for printed output
C      LUCHAN   = Logical unit holding fixed nuclei channel and target 
C                 data
C      LUKMT    = LOGICAL UNIT FOR K-MATRIX OUTPUT 
C      LUWFN    = Logical unit for R-matrix and wavefunction output
C      LUNRMT   = Logical unit holding non adiabatic R-matrix data
C      LURMT    = Logical unit holding fixed nuclei R-matrix data
C      LUVCHN   = Logical unit holding vibrational/dissociating
C                 channel data
C      MDMAX    = maximum multipole to be retained in expansion of
C                 asymptotic internuclear (dissociation) potential
C      MGVN     = TOTAL SYMMETRY OF SYSTEM
C      NAME     = TITLE FOR OUTPUT
C      NCHSET   = Set numbers for input fixed nuclei channel/target
C                 data for each geometry
C      NDIS     = NUMBER OF DISSOCIATING CHANNELS
C      NERANG   = Number of subranges of scattering energies
C      NESCAT   = NUMBER OF INPUT SCATTERING ENERGIES in each subrange
C      NEWBUT   = switch on energy parameter in Buttle correction
C      NGEOM    = NUMBER OF GEOMETRIES
C      NKSET    = Set number for output K-matrices 
C      NNRSET   = Set number for input non-adiabatic R-matrix data
C      NPOLE    = NUMBER OF ELECTRONIC R-MATRIX POLES TO BE TREATED
C                 NON-ADIABATICALLY
C      NRMSET   = Set numbers for input fixed nuclei R-matrix data for
C                 each geometry
C      NRQUAD   = NUMBER OF QUADRATURE POINTS FOR INTEGRALS IN ADIABATIC
C                 APPROXIMATION ( IF =0 THEN CODE DECIDES)
C      NVCHSET  = Set number for input vibrational/dissociating channel
C                 data
C      NVTARG   = NUMBER OF VIBRATIONAL LEVELS FOR EACH TARGET STATE
C      NWSET    = Set number for output R-matrices and wavefunctions
C      R        = ARRAY HOLDING INTERNUCLEAR SEPARATIONS
C      STOT     = SPIN MULTIPLICITY 2*S+1 WHERE S = TOTAL SPIN OF SYSTEM
C
C
C***********************************************************************
C
      DATA IREAD,LUCHAN,LURMT,LUKMT,LUWFN/5,10,21,190,0/,
     1 LUVCHN,LUNRMT/28,29/,NERANG/1/,IEUNIT/1/,
     2 NKNOT/0/,
     3 EINC/MAXENR*0.D0,MAXENR*0.D0/,NVCSET,NNRSET,NKSET,NWSET/4*1/,
     4 BBLOCH,BIGB/2*0.D0/,NVTARG/MAXTGT*1/,NRQUAD/0/
     5,NESCAT/MAXENR*10/,nbigset/1/,QMOLN/.FALSE./
      DATA ZERO/0.D0/,HALF/0.5D0/,ONE/1.D0/,TWO/2.D0/,NCOL/6/
      DATA FORM,CHFORM,RFORM,VCFORM,NRFORM,KFORM,WFORM/7*'FORMATTED'/
     1,ICFORM,IRFORM,IVCFRM,INRFRM,IKFORM,IWFORM/6*'U'/
C      DATA EUNIT/'RYD','EV'/,RYD/0.073500D0/,BLANK/'        '/
      DATA BLANK/'        '/

      DATA MODDAT/'05-Jan-2004'/
C
c
      NAMELIST/RSLVIN/LUCHAN,LURMT,LUVCHN,LUNRMT,LUKMT,LUWFN,NPOLE,NDIS,
     1                BBLOCH,BIGB,NGEOM,R,IWRITE,ISMAX,NAME,NESCAT,EINC,
     *                IPRNT,NCHSET,NRMSET,NVCSET,NNRSET,NKSET,NRQUAD,
     3                ICFORM,IRFORM,IVCFRM,INRFRM,IKFORM,NERANG,NWSET,
     4                MGVN,STOT,GUTOT,IEUNIT,NVTARG,MDMAX,IWFORM,NLPOLE,
     *                nbigset,newbut,QMOLN
c

! Sets up the calculation of K-matrices 
c
C AGS different K matrix groups 
      LUKMT = LUKMT + IAM

C---- SET UP DEFAULT VALUES OF POINTERS NCHSET AND NRMSET
      DO 111 I=1,MAXPTS
      NCHSET(I) = I
      NRMSET(I) = I
 111  continue
C

      IFAIL = 0
      NEXT = 1
      GUTOT = 0
C
C---- Read basic data via namelist /RSLVIN/
      open (unit=599,form="formatted",file="time_del.inp")
      READ(599,nml=RSLVIN,iostat=ios)
      close(599)

      IF(ICFORM.EQ.'U') CHFORM='UN'//FORM
      IF(IRFORM.EQ.'U') RFORM='UN'//FORM
      IF(INRFRM.EQ.'U') NRFORM='UN'//FORM
      IF(IKFORM.EQ.'U') KFORM='UN'//FORM
      IF(IWFORM.EQ.'U') WFORM='UN'//FORM
      IF(IVCFRM.EQ.'U') VCFORM='UN'//FORM
C
      IF(NGEOM.GT.MAXPTS) GO TO 96
C
C---- Date stamp run and print title
      CALL DATEST(DAytim)
      NAME = ' '
      NAME(61:) = DAytim
      WRITE(IWRITE,12)MODDAT,NAME,MGVN,STOT,GUTOT
      IF(NGEOM.EQ.1) THEN
        WRITE(IWRITE,20) R(1)
      ELSE
        WRITE(IWRITE,10)NDIS,(R(I),I=1,NGEOM)
      ENDIF
      IF(NCHSET(NGEOM).EQ.0.OR.NRMSET(NGEOM).EQ.0) GO TO 89
      WRITE(IWRITE,11)CHFORM,LUCHAN,(NCHSET(IG),IG=1,NGEOM)
      WRITE(IWRITE,33)RFORM,LURMT,(NRMSET(IG),IG=1,NGEOM)
      IF(NGEOM.GT.1) WRITE(IWRITE,21)VCFORM,LUVCHN,NVCSET,NRFORM,
     1LUNRMT,NNRSET
      WRITE(IWRITE,31)KFORM,LUKMT,NKSET
      IF(LUWFN.NE.0) WRITE(IWRITE,32) WFORM,LUWFN,NWSET
C
C---- Calculate total number of scattering energies, NESC and max and 
C     min energies EMIN and EMAX
!
      print*, 'Done DATEST'	

      emin=einit
      emax=efinal
      WRITE(IWRITE,13) NESC,EMIN,EMAX,EUNIT(1)
C
      eminr=emin
      emaxr=emax
C---- Find first fixed-nuclei R-matrix input set and read dimension 
C     information
      WRITE(IWRITE,17)
      print*, 'Calling READRH, LURMT =',LURMT	
      CALL READRH(LURMT,NRMSET(nbigset),RFORM,MGVN,STOT,GUTOT,NCHANF,
     1 NVIB0,NDIS0,NTARG,ION,R(1),RMASS,RMATR,NFBUT,ISMX,nstat,NOCSF,
     2 NPLX,ezero(1),iex,IWRITE,IPRNT(1),IFAIL)
      print*, 'Done READRH'	
      IF(IFAIL.NE.0) RETURN
      TWOM = TWO*RMASS
C
C---- Read header on non-adiabatic R-matrix file
      IF(NGEOM.GT.1) THEN
        WRITE(IWRITE,19)
        CALL READRH(LUNRMT,NNRSET,NRFORM,MGVN,STOT,GUTOT,NCHANS,NVIB,
     1  NDISS,NTARGv,ION,ZERO,RMASS,RMATN,NNBUT,ISMVX,nstat,NHD,
     2  NPVEC,ezero(1),iex,IWRITE,IPRNT(1),IFAIL)
        IF(IFAIL.NE.0) RETURN
        IF(NDISS.NE.NDIS.AND.NDIS.GT.0) THEN
          WRITE(IWRITE,23) NDIS,NDISS
          NDIS = NDISS
        ENDIF
        NCHAN = NCHANS
        RR = ZERO
      ELSE
        NVIB = 0
        NDIS = 0
        NCHAN = NCHANF
        ntargv = ntarg
        RR = R(1)
        ISMVX = ISMX
      ENDIF
      IF(ISMAX.EQ.-1.OR.ISMAX.GT.ISMX) THEN
        ISFMAX = ISMX
      ELSE
        ISFMAX = ISMAX
      ENDIF
      WRITE(IWRITE,34) ISFMAX
      IF(NDIS.NE.0) THEN
        IF(MDMAX.EQ.-1.OR.MDMAX.GT.ISMVX) MDMAX=ISMVX
        WRITE(IWRITE,35) MDMAX
      ENDIF
      ISMAX = MAX(ISFMAX,MDMAX)
      print*, 'Done READRH'	
C
C
C---- Assign storage for fixed nuclei data
      ntv = max(ntarg,NTARGv)
      ncf = max(NCHAN,nchanf)
      allocate (etarg(ngeom*ntv),starg(ntv),mtarg(ntv),gtarg(ntv))
      allocate (evchl(ncf),lvchl(ncf),mvchl(ncf),ivchl(ncf))
      allocate (epole(nstat*ngeom),wamp(nstat*NCHANF*NGEOM),
     * ichl(nchanf),butc(3*NCHANF*NGEOM),amc(ISMAX*NCHAN*NCHAN),
     * adc(MDMAX*NDIS*NDIS),adm(5*ndis),cf(ISMAX*ncf*(ncf+1)/2),
     * ncsf(ngeom))
      if (abs(nfbut).gt.1) then
           allocate(sfac(nchanf*ngeom),
     * ecex(iex*ngeom),rcex(iex*nchanf*ngeom))
      else
           allocate(sfac(1),
     * ecex(1),rcex(1))
      end if
C
C---- LOOP OVER GEOMETRIES
      WRITE(IWRITE,17)
      DO 2 IG=1,NGEOM
C
C---- Storage allocation for current geometry
      IG1 = IG-1
      IET = 1+IG1*NTARG
      IEG = 1+IG1*nstat
      IWA = 1+IG1*nstat*NCHANF
      IBUT =1+IG1*3*NCHANF
      isf  =1+IG1*nchanf
      iec  =1+IG1*iex
      irc  =1+IG1*iex*nchanf
C
C---- Read target and channel data 
      NCHAN0 = NCHANF
      NTARG0 = NTARG
      CALL READTC(LUCHAN,NCHSET(IG),NCHAN0,NVIB0,NDIS0,NTARG0,ION,IVT0,
     1 IVU0,ICHL,LVCHL,MVCHL,EVCHL,STARG,MTARG,GTARG,etarg(IET),R(IG),
     * RMASS,CHFORM,IWRITE,IPRNT(1),IFAIL)
      IF(NCHAN0.NE.NCHANF.OR.NTARG0.NE.NTARG) GO TO 92

	if(ntarg.gt.maxthresh-1) then
	ntarget=maxthresh-1
	else
	ntarget=ntarg	
	endif
	do i=1,ntarget
           etarget(i)=etarg(i)
	enddo
	
C
C---- Read R-matrix header for second and subsequent geometries
      CALL READRH(LURMT,NRMSET(IG),RFORM,MGVN,STOT,GUTOT,NCHAN0,NVIB0,
     * NDIS0,NTARG0,ION,R(IG),RMASS,RMATR,NFBUT,ISMX,NCSF(ig),nci,
     2 NPLX,ezero(ig),iex,IWRITE,IPRNT(1),IFAIL)
      IF(NCHAN0.NE.NCHANF.OR.NTARG0.NE.NTARG) GO TO 92
C
C---- Read remainder of fixed nuclei R-matrix data
c  bad hack fix for unwanted allocated partitioned R-matrix arrays  
      if (abs(nfbut).gt.1) then
        isf_d = isf
        iec_d = iec
        irc_d = irc
      else
        isf_d = 1
        iec_d = 1
        irc_d = 1
      end if     
      CALL READRM(LURMT,RFORM,NCHANF,NCSF(ig),nci,ISMX,isfmax,NPLX,0,
     1 NFBUT,cf,epole(IEG),wamp(IWA),vec,butc(IBUT),sfac(isf_d),
     2 iex,ecex(iec_d),rcex(irc_d),IFAIL)
      IF(IFAIL.NE.0) RETURN
C
 2    CONTINUE
C
      IF(NGEOM.EQ.1) THEN
C
C---- Set up fixed nuclei calculation
        e0 = etarg(1)
        NVCHAN = NCHAN
C
      ELSE
        WRITE(IWRITE,22)
        NVCHAN = NCHAN-NDISS
        IF(NDIS.EQ.0) NCHAN = NVCHAN
C
C---- Read vibrational channel data 
        NVIBD = NVIB+NDIS
        allocate (evib(ntargv),ivtarg(nvibd),ivnu(nvibd))
c
        CALL READTC(LUVCHN,NVCSET,NCHAN,NVIB,NDIS,NTARGv,ION,IVTARG,
     1  IVNU,IVCHL,LVCHL,MVCHL,EVCHL,STARG,
     2  MTARG,GTARG,Evib,ZERO,RMASS,VCFORM,IWRITE,IPRNT(1),IFAIL)
c
        deallocate(evib,ivtarg,ivnu)
C
        print*, 'Calling VIBINI'	
C---- Initialize acquisition of vibrational functions
        EBASE = zero
        CALL VIBINI(IREAD,IWRITE,NTARG,NVTARG,RMASS,ebase,IPRNT(2))
        WRITE(IWRITE,47) EBASE
C
C---- Set up quadrature scheme for integrals in adiabatic nuclei approx
        IF(NRQUAD.EQ.0) THEN
C     THIS IS A BIT ARBITRARY AND NOT THOROUGHLY TESTED
          NQUAD = 15*NVIB+1
          IF(MOD(NQUAD,2).EQ.0) NQUAD=NQUAD+1
        ELSE
          NQUAD = NRQUAD
        ENDIF
        allocate (rquad(nquad),qwts(nquad))
C
        print*, 'Calling VMESH'	

        CALL VMESH(R(1),R(NGEOM),NQUAD,RQUAD,QWTS)
C
C---- Initialize spline interpolation
        print*, 'Calling SPLINI'	

        CALL SPLINI(NKNOT,KNOTS,RK,MAXPTS,NGEOM,R,IWRITE)
C
C----- GET TARGET VIBRATIONAL WAVEFUNCTIONS ON QUADRATURE MESH
c
        allocate (evib(nvibd),ivtarg(nvibd),ivnu(nvibd),ichord(nvchan),
     *  vibfn(NVIB*NQUAD))
C
        print*, 'Calling RVIBR'	

       CALL RVIBR(NVIB,NQUAD,EVIB,IVTarg,IVnU,VIBFN,dum,RQUAD)
       e0 = evib(1)
C
c---- Set up pointer from VIBINI ordering to channel ordering
        if(ntarg.gt.1) call REORDI(nvchan,evchl,nvib,evib,ichord)
C
        IF(IPRNT(2).GT.0) CALL CHECKQ(NQUAD,NVIB,QWTS,VIBFN,IWRITE)
C
C---- Storage allocation for non-adiabatic data
        allocate (ampn(nstat*NCHANS),eign(nstat))
C
C---- Read rest of non-adiabatic R-matrix file
        CALL READRM(LUNRMT,NRFORM,NCHANS,nstat,NHD,ISMVX,ISMAX,0,0,0,cf,
     1  EIGn,AMPn,dum,dum,dum,0,dum,dum,IFAIL) 
C
      ENDIF
C
C---- Save multipole coefficients as square matrix
      IF(ISMAX.GT.0) THEN
        CALL SQUARM(NVCHAN,ISMAX,cf,AMC)
        IF(NDIS.GT.0.AND.MDMAX.GT.0) THEN
C---- Unpack dissociation potential data.  This code must match
C     DISINI in VIBRMT
          ITEMPD = ISMAX*NVCHAN*(NVCHAN+1)/2+1
          ND2 = NDIS*(NDIS+1)/2+6*NDIS
          CALL SPLITM(NDIS,ND2,MDMAX,cf(ITEMPD),ADC,ADM)
        ENDIF
      ENDIF
      deallocate (cf)
C
C----- INITIALIZE ASYMPTOTIC ROUTINES FOR VIBRATIONAL CHANNELS
      IF(NVCHAN.GT.0) THEN
        RAFINV = RMATR
        SCALE = ONE
	print*,'calling asym1'
        CALL ASYM1(NVCHAN,LVCHL,ION,ISMAX,AMC,RMATR,RAFINV,
     1  SCALE,BBLOCH,EVCHL,EMINR,EMAXR,IVPROP,POTL,IWRITE)
      ENDIF
       !print*,NVCHAN,LVCHL,ION,ISMAX,AMC,RMATR,RAFINV,
       !   SCALE,BBLOCH,EVCHL,EMINR,EMAXR,IVPROP,IWRITE
	print*,'end of asym1 '
C
C----- INITIALIZE ASYMPTOTIC ROUTINES FOR DISSOCIATING CHANNELS
      IF(NDIS.GT.0) THEN
        EMINM = TWOM*EMINR
        EMAXM = TWOM*EMAXR
        IF(IVPROP.EQ.0) IDPROP=0
        RAFIND = RMATN
        SCALE = ONE/TWOM
        CALL ASYM1(NDIS,LVCHL(1+NVCHAN),0,MDMAX,ADC,RMATN,RAFIND,
     1  SCALE,BIGB,EVCHL(1+NVCHAN),EMINM,EMAXM,IDPROP,DISPOT,IWRITE)
        IF(IDPROP.NE.IVPROP) GO TO 94
      ELSE
        IDPROP = 0
      ENDIF
C
        print*, 'Done asym'	

C----- INITIALIZE OUTPUT OF K-MATRICES 
      IF(LUKMT.NE.0) CALL WRITKH(LUKMT,NKSET,KFORM,NAME,MGVN,STOT,
     1 GUTOT,ION,RR,RMASS,NCHAN,NVIB,NDIS,NTARG,NERANG,NESCAT,EINR,
     2 NESC,IPRNT(6),IWRITE,IFAIL)
C
C----- Initialize output of R-matrices and wavefunctions
      IF(LUWFN.NE.0) CALL WRITWH(LUWFN,NWSET,WFORM,NAME,MGVN,STOT,
     1 GUTOT,ION,RR,RMASS,NCHAN,NVIB,NDIS,NTARG,NERANG,NESCAT,EINR,
     2 NESC,IPRNT(6),IWRITE,IFAIL)
C
C---- Store Bloch coefficients
      allocate (bloch(nvchan+ndis))
      DO 4 I=1,NVCHAN
      BLOCH(I) = BBLOCH
 4    continue
      DO 5 I=1,NDIS
      BLOCH(NVCHAN+I) = BIGB 
5     continue
C-----------------------------------------------------------------------
C
      return
 89   WRITE(IWRITE,91) NGEOM,NCHSET(NGEOM),NRMSET(NGEOM),R(NGEOM)
 91   FORMAT(/' ERROR IN GEOMETRY RELATED DATA'/' NGEOM =',I3,'  NCHSET(
     1NGEOM) =',I3,'  NRMSET(NGEOM) =',I3,'  R(NGEOM) =',F6.3)
      GO TO 90
 92   WRITE(IWRITE,93) NCHAN0,NCHAN,NTARG0,NTARG
 93   FORMAT(' INCONSISTENT DATA ON INPUT FILES'/' NCHAN0 =',I5,5X,'NCHA
     1N =',I5,5X,'NTARG0 =',I5,5X,'NTARG =',I5)
      GO TO 90
 94   WRITE(IWRITE,95)IVPROP,IDPROP
 95   FORMAT(/' INCONSISTENT PROPAGATION FLAGS',2I5)
      GO TO 90
 96   WRITE(IWRITE,98) NTARG,NGEOM,MAXTGT,MAXPTS
 98   FORMAT(/' INPUT DATA WILL EXCEED FIXED DIMENSIONS'/' INPUT  ',
     12I5/' MAXIMA ',2I5)
 90   IFAIL = 1
      RETURN
C
 10   FORMAT(/' Vibrationally resolved calculation '//' Number of dissoc
     2iating channels',I3//' Input geometries R =',10F10.5,(/21X,10F10.5
     2))
 11   FORMAT(/' Input datasets:',33X,'Unit  Set numbers'/
     1' Target and channel data     LUCHAN (',A11,')',I3,5X,30I3/(35X,30
     2I3))
 12   FORMAT(//' Program TIMEDEL  (last modified ',A,' )'//A//
     1' Symmetry data  MGVN =',I2,' STOT =',I2,' GUTOT =',I2)
 13   FORMAT(/' K-matrices will be calculated for',I5,' energies in the 
     1range [',F8.4,',',F8.4,'] ',A)
 14   FORMAT(/' SUPER R-MATRIX')
 15   FORMAT(/' K-MATRIX')
 16   FORMAT(I3,12F10.5/(3X,12F10.5))
 17   FORMAT(/' *** FIXED NUCLEI DATA ***')
 18   FORMAT(/' *** Task successfully completed ***')
 19   FORMAT(/' *** NON-ADIABATIC DATA ***')
 20   FORMAT(/' Fixed nuclei calculation for R =',F6.3)
 21   FORMAT(/' Vibrational channel data    LUVCHN (',A11,')',I3,5X,I3
     1/' Non-adiabatic R-matrix data LUNRMT (',A11,')',I3,5X,I3)
 22   FORMAT(/' *** END OF FIXED NUCLEI DATA *** ')
 23   FORMAT(/' NDIS =',I2,' IS INCOMPATIBLE WITH DATA FROM VIBRMT',2X,
     1'CHANGED TO ',I2)
 24   FORMAT(/' CONTRIBUTION TO ELECTRONIC R-MATRIX FROM FIRST',I3,
     1' POLES')
 25   FORMAT(/' COUPLING R-MATRIX')
 26   FORMAT(/' NUCLEAR MOTION R-MATRIX')
 27   FORMAT(10A8)
 28   FORMAT(/100('-')//' INCIDENT ENERGY',F10.5,' RYD')
 31   FORMAT(/' Output datasets:',32X,'Unit  Set number'/
     1' K-matrices',18X,'LUKMT  (',A11,')',I3,5X,I3)
 32   FORMAT(' Wavefunction data           LUWFN  (',A11,')',I3,5X,30I3/
     1(35X,30I3))
 33   FORMAT(' Fixed nuclei R-matrix data  LURMT  (',A11,')',I3,5X,30I3/
     1(35X,30I3))
 34   FORMAT(/' Maximum multipole USED in asymptotic scattering potentia
     1ls   ISMAX =',I3)
 35   FORMAT(/' Maximum multipole USED in asymptotic dissociating potent
     1ials MDMAX =',I3)
 38   FORMAT(/' Adiabatic approximation to contribution to vibrational R
     1-matrix from higher poles failed at E =',F7.4,' Ryd'/' If higher e
     2nergies are required, increase NPOLE')
 39   FORMAT(/' Adiabatic approximation to contribution to vibrational R
     1-matrix from lowest poles failed at E =',F7.4,' Ryd'/' If lower en
     2ergies are required, decrease NLPOLE')
 47   FORMAT(/' Base energy used in nuclear motion code  EBASE =',F11.5,
     1' au')
C
      end
!
      subroutine getkmat(maxnopen,energy,kmt,dumnopen)
      use userdefkmat
      implicit double precision (a-h,o-z)
! Calculates a K-matrix at given input energy. Returns the
! K-matrix and the number of open channels.
! Note that the K-matrix returned must contain all the elements
! within (1:nopen,1:nopen)
      integer :: maxnopen,dumnopen,ifail
      double precision :: kmt(maxnopen,maxnopen)
      double precision, allocatable :: rvib(:),rmn(:),fx(:),fxp(:),
     * akmat(:),rres(:),fv(:),fvp(:),fd(:),fdp(:),crv(:),crd(:)
      integer :: ien
      DATA HALF/0.5D0/,NCOL/10/
!
C---- Storage allocation for energy loop
C---- Storage allocation for energy loop
      ien = 0
! ***** ien isn't used in the timedel calculation
      NCHSQ= NCHAN*NCHAN
      NVCHSQ = NVCHAN*NVCHAN
      allocate (rvib(nchsq),fx(2*nchsq),fxp(2*nchsq),fv(2*nvchsq),
     * fvp(2*nvchsq),fd(2*ndis*ndis),fdp(2*ndis*ndis),akmat(nchsq),
     * rres(NGEOM*NCHANF*(NCHANF+1)/2),crv(2*NVCHSQ+NVCHAN),
     * crd(NDIS*(2*NDIS+1))) 
      if(npole.gt.0) allocate (rmn(NCHAN*(NCHAN+1)/2))
C
c
	
      enryd=energy
	!print*,'e0',e0
	!print*,'enryd',enryd
      ETOT  = e0+HALF*ENRYD
	!print*, 'etot',etot
      IF(IPRNT(5).GT.0.OR.IPRNT(6).GT.0) WRITE(IWRITE,28) ENRYD
C
      NVOPEN = 0
      NDOPEN = 0
      ifail = 0
C
C---- Calculate contribution to R-matrix from non-adiabatic poles
      IF(NPOLE.GT.0) THEN
		!print*,'calling vrmat2'
        CALL VRMAT2(NCHAN,NHD,RMN,ETOT,AMPn,EIGn,NLPOLE)
		!print*,'exited vrmat2'
        IF(IPRNT(5).NE.0) THEN
          WRITE(IWRITE,24) NPOLE
          CALL MATTPT(NCHAN,RMN,IWRITE)
        ENDIF
      ENDIF
C
C----- CALCULATE CONTRIBUTIONS TO FIXED NUCLEI R-MATRICES FROM HIGHER
C      POLES
      if(newbut.eq.0) nfbut=-nfbut
!NV-03
!       write(6,*)
!       write(6,*)'using residr, newbut=',newbut 
      if(newbut.eq.0) nfbut=-nfbut
		!print*,'calling residr'
      CALL RESIDR(ETOT,NCHANF,NTARG,ETARG,NLPOLE,NPOLE,nstat,
     1     NGEOM,ncsf,ichl,WAMP,EPOLE,NFBUT,BUTC,RRES,ezero,sfac,
     2     iex,ecex,rcex,IWRITE,IFAIL)
		!print*,'exited residr'
      IF(IFAIL.NE.0) THEN
        IF(IEUNIT.EQ.2) Entop = Enryd/RYD
        IF(IFAIL.EQ.1) THEN
          WRITE(IWRITE,39) ENtop,eunit(ieunit)
          IFAIL = 0
          GO TO 50
        ELSE
          WRITE(IWRITE,38) ENtop,eunit(ieunit)
          IFAIL = 0
          GO TO 40
        ENDIF
      ELSE
        IEN = IEN+1
      ENDIF
C
C
C----- CALCULATE RESIDUAL R-MATRIX IN THE ADIABATIC NUCLEI APPROX. AND
C      ADD IT TO THE VIBRATIONAL R-MATRIX OBTAINED IN VRMAT2.  A SQUARE
C      MATRIX IS OUTPUT FOR INPUT TO ASYMPTOTIC CODE.
C
		!print*,'npole',npole
      IF(npole.GT.0) THEN
		!print*,'calling adnuc'
        CALL ADNUC(NGEOM,R,NCHANF,ICHL,NTARG,NVTARG,VIBFN,KNOTS,
     1  RK,NVCHAN,RRES,RVIB,dum,1,NQUAD,RQUAD,QWTS)
		!print*,'exiting adnuc'
c
c---- Reorder elements to match channel labels
		!print*,'ntarg',ntarg
	if(ntarg.gt.1) then
		!print*,'calling reordv'
         call REORDV(nvchan,ichord,rvib)
		!print*,'exiting reordv'
	endif
C     Add adiabatic component of R-matrix to the non-adiabatic
        K = 0
        IJ = 0
        DO 71 I=1,NVCHAN
        DO 7 J=1,I
        K = K+1
        IJ = IJ+1
        rmn(K) = rmn(K)+rvib(IJ)
 7      continue
 71     continue
	!print*,'calling squarm'
        CALL SQUARM(NCHAN,1,RMN,RVIB)
	!print*,'exiting squarm'
      ELSE
	!print*,'calling squarm in else'
        CALL SQUARM(NCHAN,1,RRES,RVIB)
      ENDIF
C
	!print*, 'rvib squarm'
	!print*,rvib
	!print*, 'rvib squarm end'
      IF(IPRNT(5).GT.0) THEN
        WRITE(IWRITE,14)
        CALL WRECMT(RVIB,NCHAN,NCHAN,NCHAN,NCHAN,NCOL,IWRITE)
      ENDIF
C----- GET SOLUTIONS, DERIVATIVES AND GLOBAL R-MATRIX IN VIBRATIONAL
C      CHANNELS AT R=RAFINV
	!	print*,'asym2 print start'
	!print*,'NVCHAN',NVCHAN
	!print*,'NVOPEN',NVOPEN
	!print*,'LVCHL',LVCHL
	!print*,'ION',ION
	!print*,'ISMAX',ISMAX
	!print*,'AMC',AMC
	!print*,'CRV',CRV
        !print*,'RAFINV',RAFINV
	!print*,'EVCHL',EVCHL
	!print*,'ENRYD',ENRYD
	!print*,'FV',FV
	!print*,'IVPROP',IVPROP
	!print*,'ifail',ifail
		!print*,'asym2 print end'
      IF(NVCHAN.GT.0) THEN
		!print*,'calling asym2 R=RAFINV'
        CALL ASYM2(NVCHAN,NVOPEN,LVCHL,ION,ISMAX,AMC,CRV,
     1  RAFINV,EVCHL,ENRYD,FV,FVP,IVPROP,ifail)
        if(ifail.gt.1) write(6,*) 
     1  'failed on return from asym2 for R=RAFINV'
        ifail = 0
      ENDIF
		!print*,'asym2 exit print start'
	!print*,'NVCHAN',NVCHAN
	!print*,'NVOPEN',NVOPEN
	!print*,'LVCHL',LVCHL
	!print*,'ION',ION
	!print*,'ISMAX',ISMAX
	!print*,'AMC',AMC
	!print*,'CRV',CRV
        !print*,'RAFINV',RAFINV
	!print*,'EVCHL',EVCHL
	!print*,'ENRYD',ENRYD
	!print*,'FV',FV
	!print*,'IVPROP',IVPROP
	!print*,'ifail',ifail
		!print*,'asym2 exit print end'
C
C----- GET SOLUTIONS, DERIVATIVES AND GLOBAL R-MATRIX IN DISSOCIATING
C      CHANNELS AT R=RAFIND
      IF(NDIS.GT.0) THEN
        E2M = RMASS*ENRYD
		!print*,'calling asym2 R=RAFIND'
        CALL ASYM2(NDIS,NDOPEN,LVCHL(1+NVCHAN),ION,MDMAX,ADC,
     1  CRD,RAFIND,EVCHL(1+NVCHAN),E2M,FD,FDP,IDPROP,ifail)
        if(ifail.gt.1) write(6,*) 
     1  'failed on return from asym2 for R=RAFIND'
        ifail = 0
C
C----- IF NO PROPAGATION MUST USE NUMERICAL INTEGRATION
        IF(IDPROP.EQ.0.AND.RAFIND.GT.R(NGEOM)) then
		!print*,'calling asymd'
	 CALL ASYMD(E2M,NDIS,
     1  TWOM,R(NGEOM),RAFIND,EVCHL(1+NVCHAN),FD,FDP,ADM,
     2  IWRITE,IPRNT(3))
	endif
      ENDIF
C
C----- MERGE SOLUTIONS AND DERIVATIVES
      NOPEN = NVOPEN+NDOPEN
      if(nopen.eq.0) go to 40
	!print*,'calling merge'
      CALL MERGE(NCHAN,NVCHAN,NDIS,NVOPEN,NDOPEN,FX,FXP,FV,FVP,FD,FDP)
C
	!print*, 'start of print before rpropx'
	!print*,NCHAN,NVCHAN,NDIS
	!print*,'crv',CRV
	!print*,'crd',CRD
	!print*,'RVIB',rvib
	!print*,IPRNT(5),IWRITE,IBACK
		!print*, 'end of print before rpropx'
      if(RMATR.gt.RAFINV) IBACK=-1
C----- PROPAGATE R-MATRICES IF REQUIRED
      IF(IDPROP.GT.0.OR.IVPROP.GT.0) then
		!print*,'calling rpropx'
        CALL RPROPX(NCHAN,NVCHAN,NDIS,CRV,CRD,RVIB,IPRNT(5),IWRITE,
     2  IBACK)
	endif
 
C----- COMPUTE K-MATRIX
	!print*,'here'
	!print*,'nchan'
	!print*,NCHAN
	!print*,'bloch'
	!print*,BLOCH
	!print*,'NOPEN'
	!print*,nopen
	!print*,'fx'
	!print*,FX
	!print*,'fxp'
	!print*,FXP
	!print*,'RVIB'
	!print*,rvib
	!print*,'akmat'
	!print*,AKMAT
	!print*,'2here'
      CALL KMAT(NCHAN,BLOCH,NOPEN,FX,FXP,RVIB,AKMAT)

	!print*, akmat
C

      IF(IPRNT(6).GT.0) THEN
        WRITE(IWRITE,15)
        CALL WRECMT(AKMAT,NOPEN,NOPEN,NOPEN,NOPEN,NCOL,IWRITE)
      ENDIF
!NV-03
      if (lukmt1 > 0) call writkm (nopen,ndopen,enryd,akmat)
C
	!print*,'-------------------------------------'
 40   continue
 50   CONTINUE
      dumnopen=nopen
      do i=1,nopen
         do j=1,nopen
            kmt(i,j)=akmat(nopen*(i-1)+j)
         enddo
      enddo       
      return
		
!
!
 90   IFAIL = 1

      RETURN
C
 10   FORMAT(/' Vibrationally resolved calculation '//' Number of dissoc
     2iating channels',I3//' Input geometries R =',10F10.5,(/21X,10F10.5
     2))
 11   FORMAT(/' Input datasets:',33X,'Unit  Set numbers'/
     1' Target and channel data     LUCHAN (',A11,')',I3,5X,30I3/(35X,30
     2I3))
 13   FORMAT(/' K-matrices will be calculated for',I5,' energies in the 
     1range [',F8.4,',',F8.4,'] ',A)
 14   FORMAT(/' SUPER R-MATRIX')
 15   FORMAT(/' K-MATRIX')
 16   FORMAT(I3,12F10.5/(3X,12F10.5))
 18   FORMAT(/' *** Task successfully completed ***')
 19   FORMAT(/' *** NON-ADIABATIC DATA ***')
 23   FORMAT(/' NDIS =',I2,' IS INCOMPATIBLE WITH DATA FROM VIBRMT',2X,
     1'CHANGED TO ',I2)
 24   FORMAT(/' CONTRIBUTION TO ELECTRONIC R-MATRIX FROM FIRST',I3,
     1' POLES')
 25   FORMAT(/' COUPLING R-MATRIX')
 26   FORMAT(/' NUCLEAR MOTION R-MATRIX')
 27   FORMAT(10A8)
 28   FORMAT(/100('-')//' INCIDENT ENERGY',F10.5,' RYD')
 31   FORMAT(/' Output datasets:',32X,'Unit  Set number'/
     1' K-matrices',18X,'LUKMT  (',A11,')',I3,5X,I3)
 34   FORMAT(/' Maximum multipole USED in asymptotic scattering potentia
     1ls   ISMAX =',I3)
 35   FORMAT(/' Maximum multipole USED in asymptotic dissociating potent
     1ials MDMAX =',I3)
 38   FORMAT(/' Adiabatic approximation to contribution to vibrational R
     1-matrix from higher poles failed at E =',F7.4,' Ryd'/' If higher e
     2nergies are required, increase NPOLE')
 39   FORMAT(/' Adiabatic approximation to contribution to vibrational R
     1-matrix from lowest poles failed at E =',F7.4,' Ryd'/' If lower en
     2ergies are required, decrease NLPOLE')
 47   FORMAT(/' Base energy used in nuclear motion code  EBASE =',F11.5,
     1' au')
C
      return
      end
