module noise_mod
      use precisn, only: wp
      implicit none

  contains

      subroutine filter(array_x, array_y, window, multiple, neig, N)
          implicit none
          real(wp), intent(inout), allocatable :: array_x(:), array_y(:)
          real(wp), allocatable :: temp_x(:), temp_y(:,:)
          real(wp), intent(in) :: multiple
          integer, intent(in) :: window, neig, N
          integer :: i, j, kept, deleted, ifail

          allocate(temp_x(N), temp_y(neig,N))
          temp_x = 0.
          temp_y = 0.

          deleted = 0
          kept = 0

          do i = 1, N
              if ((i.le.window).or.(i.gt.(N-window))) then
                  kept = kept + 1
                  temp_x(kept) = array_x(i)
                  do j = 1, neig
                      temp_y(j,kept) = array_y(i+(j-1)*N)
                  enddo
              else
                  if (noise(array_y(i-window:i+window), window)) then
                      deleted = deleted + 1
                      write(6, '(a,2(f12.4,x))') 'deleted point: ', array_x(i), array_y(i)
                  else
                      kept = kept + 1
                      temp_x(kept) = array_x(i)
                      do j = 1, neig
                          temp_y(j,kept) = array_y(i+(j-1)*N)
                      enddo
                  endif
              endif

          end do

          deallocate(array_x, array_y, stat=ifail)
          if (ifail.ne.0) print *, 'ERROR: in subroutine filter() cannot deallocate arrays'

          allocate(array_x(kept), array_y(neig*kept))

          array_x = temp_x(1:kept)
          do j = 1, neig
              array_y(1+(j-1)*kept:j*kept) = temp_y(j,1:kept)
          enddo

          write(6, '(a,x,I0)') 'number of data points deleted : ', deleted
          write(6, '(a,x,e12.4)') 'Percentage of data deleted (%): ', deleted/real(N)

          deallocate(temp_x, temp_y)

      end subroutine filter

      function mean(array)
          real(wp) :: mean
          real(wp), intent(in) :: array(:)

          mean = sum(array)/real(size(array))
      end function mean

      function std_dev(array)
          real(wp) :: std_dev
          real(wp), intent(in) :: array(:)
          real(wp) :: mu, var

          mu = mean(array)
          var = sum((array - mu)*(array - mu))
          var = var / real(size(array) - 1)
          std_dev = sqrt(var)

      end function std_dev

      function noise(array, window)
          logical :: noise
          real(wp), intent(in) :: array(:)
          real(wp) :: cutoff, current
          integer, intent(in) :: window
          logical :: c1, c2

          cutoff = max(std_dev(array), 1.)
          current = array(window+1) - mean(array)

          noise = abs(current).gt.cutoff

      end function noise

end module
