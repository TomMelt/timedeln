      module serial_parallel
      use precisn, only: wp, int_sp
      use sgc_mod, only: sgc
      use comm_mpi, only: myrank, com_size, stop_mpi, set_barrier, &
     &                    set_new_comm, split_comm, distribute_integer, &
     &                    print_comm_info, distribute_array
      implicit none

      integer(kind=int_sp), save  :: geom_comm, kmat_comm, sgc_comm, jgroup, jcolour
      integer, save    :: my_rank, comm_size ! keep explicit mpi out of ptimedel

      ! shared counter type
      type(sgc), pointer, save :: pctr1
      logical, save    :: sgc_choice_tf
! choice of sgc, does task 0 have to do nothing(?)  
      integer, save    :: ithresh_cnt   ! for round robin choice
      integer, save    ::  mpi_tasks_per_geom, mpi_tasks_per_td 
! info on whether the geometry communicator is split further

      integer, save    :: istore_ieunit  

! first_or_last_sleeper hard-coded: true for first, false for last 
!  (MPI-IO window task for sgc_0_sleeper = .true.)
      logical, parameter  :: first_or_last_sleeper = .false.

      private
      public my_rank, comm_size, geom_comm, kmat_comm, sgc_comm
      public mpi_tasks_per_td, jgroup
! these are needed in ptimedel for book-keeping
      public istore_ieunit
! needed only for parallel test run with read in data

      public set_up_sgc, get_ithresh, close_sgc
      public timedel_get_rank, stop_run, further_split_check, split_for_kmat
      public distribute_two_integers, distribute_namelist_arrays, &
     &       distribute_one_array, distribute_two_arrays

      public first_or_last_sleeper

      contains 
       
! simple MPI-based routines
   subroutine timedel_get_rank(sgc_choice)
      implicit none
      logical, intent(in), optional   :: sgc_choice

      if (present(sgc_choice)) sgc_choice_tf = sgc_choice
      my_rank = myrank
      comm_size = com_size
      write(*,'(a,i8,a,i8)') ' Calling MPI from ', my_rank, ' of ', comm_size
      call set_barrier(geom_comm)
   end subroutine timedel_get_rank
   subroutine stop_run()
      implicit none
      call stop_mpi()
   end subroutine stop_run 
 

! sgc-based routines
   subroutine set_up_sgc
      implicit none
      if (jgroup == 0) then  
         if (sgc_choice_tf) then      ! use scalable global counter    
!            pctr1 => sgc(geom_comm)
            pctr1 => sgc(sgc_comm)
         else                      ! round robin choice
            ithresh_cnt = 0
         end if
      end if
   end subroutine set_up_sgc

   subroutine get_ithresh (ithresh)
      implicit none
      integer, intent(out)   :: ithresh
      integer(kind=int_sp), parameter   :: iroot = 0
      integer(kind=int_sp)              :: ithresh_i4
      if (jgroup == 0) then  
         if (sgc_choice_tf) then      ! use scalable global counter    
            ithresh_i4 = pctr1%cnt()
         else                      ! round robin choice
            ithresh_i4 = MOD(my_rank, comm_size) + (comm_size*ithresh_cnt)
! uses my_rank, comm_size, assumed set to sgc_comm values
            ithresh_cnt = ithresh_cnt + 1
         end if
      end if
! note that in both cases ithresh is defined from 0 here deliberately

      if (mpi_tasks_per_td > 1) &
     &      call distribute_integer(ithresh_i4, kmat_comm, iroot)
! ithresh distributed among kmat_comm
      ithresh = ithresh_i4
      return
   end subroutine get_ithresh
  
   subroutine close_sgc()
      implicit none
      if (jgroup == 0) then
         if (sgc_choice_tf) then          ! destroy counter (implicit barrier) 
!            call pctr1%del(geom_comm)  
            call pctr1%del(sgc_comm)  
         else                          ! round robin choice
!            call set_barrier(geom_comm)           
            call set_barrier(sgc_comm)           
         end if
      end if
      call set_barrier(geom_comm)           
   end subroutine close_sgc

! routines dealing with further split for k-matrix evaluation
   subroutine further_split_check (mpi_tasks_per_geom_in, mpi_tasks_per_td_in)
! info from main header program
      use comm_mpi, only: local_mpi_comm
      implicit none
      integer, intent(in) :: mpi_tasks_per_geom_in, mpi_tasks_per_td_in
      geom_comm = local_mpi_comm
      mpi_tasks_per_td = mpi_tasks_per_td_in
      mpi_tasks_per_geom = mpi_tasks_per_geom_in
! Note: geom_comm is used and sewt in the main program
   end subroutine further_split_check

   subroutine split_for_kmat (sgc_0_sleeper, jgroup_test)
! needs to be called from ptimedel after possible read-in of sgc_0_sleeper
      implicit none
      logical, intent(in) :: sgc_0_sleeper
      logical, intent(out) :: jgroup_test
      integer mpi_geom_temp, j_test, my_j_rank, ier
! uses module variables jcolour, jgroup, geom_comm, kmat_comm, sgc_comm,
! uses module variables mpi_tasks_per_geom, mpi_tasks_per_td

  if (mpi_tasks_per_td /= 1) then

     mpi_geom_temp = mpi_tasks_per_geom
     if (sgc_0_sleeper) mpi_geom_temp = mpi_geom_temp - 1
     j_test = MOD(mpi_geom_temp,mpi_tasks_per_td)
     if (j_test/=0) then
        if (myrank == 0) then
           if (sgc_0_sleeper) then
              write(*,*) ' *** ERROR *** Geom-kmat Task ratio incorrect: ', & 
     &        'make Kmat tasks a factor of (geom tasks - 1). Ending Run. '
           else
              write(*,*) ' *** ERROR *** Geom-kmat Task ratio incorrect: ', & 
     &        'make Kmat tasks a factor of geom tasks. Ending Run. '
           end if
        end if
        call stop_mpi
     end if 
! labels for:
!       split into kmatrix tasks groups (jcolour) 
!              and internal tasks (jgroup): 
!             jgroup = 0 actually does the timedel work
!  sgc_0_sleeper = .true. implies sgc is used AND task 0/comm_size 
!  (jgroup=jcolour=0) has to sleep due to bad MPI-2/3 implementation
     my_j_rank = myrank
     if (sgc_0_sleeper .and. first_or_last_sleeper) my_j_rank = my_j_rank - 1
     jcolour = my_j_rank / mpi_tasks_per_td
     jgroup = MOD (my_j_rank, mpi_tasks_per_td)
     if (sgc_0_sleeper) then
        jcolour = jcolour + 1
        if (first_or_last_sleeper) then ! myrank = 0 is the window task
           if (myrank == 0) then
              jcolour = 0
              jgroup = 0
           end if
        else                            ! myrank = com_size is the window task
           if ((myrank + 1) == mpi_tasks_per_geom) jcolour = 0
        end if 
     end if
     write(*,*) 'I am ', myrank, ' mpi_task_per_geom =',  mpi_tasks_per_geom
     write(*,*) 'I am ', myrank, ' jcolour, jgroup =', jcolour, jgroup 
! split communicator
! first form the kmat_comm communicator
     call split_comm (jcolour, jgroup, kmat_comm, parent_comm=geom_comm)
! now form the communicator between kmat_comm communicatora
! (this communicator is only used by the kmat_comm group leaders, jgroup = 0
     call split_comm (jgroup, jcolour, sgc_comm, parent_comm=geom_comm)
! note that split_comm resets the working communicator to the new comunicator

! note: jgroup is also needed in the parallel part of ptimedel,
!       so that sgc is only called for jgroup = 0
!      An MPI_BCAST of 'ithresh' to kmat_comm is also needed
!      Apart from this, kmat_comm is only used/passed to the (isolated) 
!      calls to share input data and  to get_kmat and setupkmat
!  The writes to unit 6 and other fort files are only done if jgroup=0
     call print_comm_info
     my_rank = myrank
     comm_size = com_size
     write(*,*) 'I am ', myrank, my_rank, ' mpi_task_per_geom =',  mpi_tasks_per_geom
     write(*,*) 'I am ', myrank, my_rank, ' jcolour, jgroup =', jcolour, jgroup 
  else
     jgroup = 0
     jcolour = 0
!     kmat_comm = MPI_COMM_NULL  ! dummy value:   
     kmat_comm = -999999 ! dummy value:   
     sgc_comm = geom_comm
  end if 
        jgroup_test = .false.
      if (jgroup == 0) jgroup_test = .true.
     write(*,*) 'I am ', myrank, my_rank, ' jcolour, jgroup, jgroup_test =', jcolour, jgroup_test, jgroup 

  return
  end subroutine split_for_kmat 

! data handling parallel routines

      subroutine distribute_two_integers (ifail, i_sgc, comm)
      implicit none
! wrapper routine for a specific distribute_array call 
! after intial namelist read by geom_comm group leader
! distributes to working communicator, in this case the full geom_comm
      integer, intent(inout) :: ifail, i_sgc
      integer(kind=int_sp), intent(in), optional  :: comm
      integer(kind=int_sp)              :: i_temp(2), n, commun

      i_temp(1) = ifail
      i_temp(2) = i_sgc
      n = 2
      if (present(comm)) then
         commun = comm
         call distribute_array (n, i_temp, commun)
      else
         call distribute_array (n, i_temp)
      end if
      ifail = i_temp(1) 
      i_sgc = i_temp(2)
      end subroutine distribute_two_integers
      
      subroutine distribute_namelist_arrays (num_list_int, dum_list_int, &
     &     num_list_real, dum_list_real, num_list_logical, dum_list_logical, &
     &     comm_d, proc_d)
      implicit none
! distribute the namelist values in three arrays, 
! to working communicator or specified communicator from rank 0 
! (easily generalized to other ranks)
! wrapper routine to avoid main code calling comm_mpi
      integer, intent(in)    :: num_list_int, num_list_real, num_list_logical
      integer, intent(inout) :: dum_list_int(num_list_int)
      real(wp), intent(inout)  :: dum_list_real(num_list_real)
      logical, intent(inout)   :: dum_list_logical(num_list_logical)
      integer(kind=int_sp), intent(in), optional    :: comm_d
      integer, intent(in), optional    :: proc_d
      integer(kind=int_sp)                :: commun 
      integer(kind=int_sp)                :: proc_v 
      integer(kind=int_sp)                :: n_list_int, n_list_real, n_list_logical     
      integer(kind=int_sp)                :: dumm_list_int(num_list_int)
      logical(kind=int_sp)                :: dumm_list_logical(num_list_logical)

      n_list_int = num_list_int
      n_list_real = num_list_real
      n_list_logical = num_list_logical
      dumm_list_int(1:n_list_int) = dum_list_int(1:num_list_int)
      dumm_list_logical(1:n_list_logical) = dum_list_logical(1:num_list_logical)
      if(present(proc_d)) then
         proc_v = proc_d
         if (present(comm_d)) then
            commun = comm_d
            call distribute_array (n_list_int, dumm_list_int, comm = commun, proc = proc_v)
            call distribute_array (n_list_real, dum_list_real, comm = commun, proc = proc_v)
            call distribute_array (n_list_logical, dumm_list_logical, comm = commun, proc = proc_v)
         else
            call distribute_array (n_list_int, dumm_list_int, proc = proc_v)
            call distribute_array (n_list_real, dum_list_real, proc = proc_v)
            call distribute_array (n_list_logical, dumm_list_logical, proc = proc_v)
         end if
      else
         if (present(comm_d)) then
            commun = comm_d
            call distribute_array (n_list_int, dumm_list_int, comm = commun)
            call distribute_array (n_list_real, dum_list_real, comm = commun)
            call distribute_array (n_list_logical, dumm_list_logical, comm = commun)
         else
            call distribute_array (n_list_int, dumm_list_int)
            call distribute_array (n_list_real, dum_list_real)
            call distribute_array (n_list_logical, dumm_list_logical)
         end if
      end if
      dum_list_int(1:num_list_int) = dumm_list_int(1:n_list_int)
      dum_list_logical(1:num_list_logical) = dumm_list_logical(1:n_list_logical)
      end subroutine distribute_namelist_arrays
 
      subroutine distribute_one_array (nop, km, com)
! possibly a wrapper too far, this just ccalls distribute array in comm_mpi
! It avoids the appearance of integer(kind=int_sp) outside serial_prallel
      implicit none
      integer, intent(in) :: nop
      real(wp), intent(inout) :: km(nop)
      integer(kind=int_sp), intent(in), optional  :: com
      integer(kind=int_sp)              :: n, commun

      n = nop
      if (present(com)) then
         commun = com
         call distribute_array(n, km, comm=commun) 
      else
         call distribute_array(n, km) 
      end if
      end subroutine distribute_one_array

      subroutine distribute_two_arrays (nop, maxnop, kmat1, kmat2, &
     &                                  iproc_1, iproc_2, com)
! wrapper routine for a specific distribute_array call 
! after intial namelist read by geom_comm group leader
! distributes to working communicator, in this case the full geom_comm
      implicit none
      integer, intent(in) :: nop, maxnop, iproc_1, iproc_2
      real(wp), intent(inout) :: kmat1(maxnop,maxnop), kmat2(maxnop,maxnop)
      integer(kind=int_sp), intent(in), optional  :: com
      integer(kind=int_sp)              :: n, nd, id1, id2, commun

      n = nop
      nd = maxnop
      id1 = iproc_1
      id2 = iproc_2

      if (present(com)) then
         commun = com
         call distribute_array(n, n, nd, kmat1, comm=commun, proc=id1) 
         call distribute_array(n, n, nd, kmat2, comm=commun, proc=id2)
      else
         call distribute_array(n, n, nd, kmat1, proc=id1) 
         call distribute_array(n, n, nd, kmat2, proc=id2)
      end if
      end subroutine distribute_two_arrays

      end module serial_parallel
