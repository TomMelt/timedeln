module sgc_mod
! Global counters using MPI-2.0 Passive RMA

! An RMA window on processor 0 is used as the counter window.

! To ensure the counter scales with processor number a tree
! is used to hold counts from each processor. The actual count is
! obtained by updating and summing the nodes from the origin
! of the tree to a particular processor leaf
!  use mpi
  
  use error_prt, only: eprt, ecode, emsg
  implicit none
  include 'mpif.h'

  private
  public sgc

  type sgc        ! Scalable Glocal Counter type
     private
     integer, allocatable :: cntr(:)      ! counter window
     integer              :: win          ! counter window handle
     integer              :: rank         ! processor rank
     integer              :: com_size     ! total # processors in comm
     integer              :: get_type     ! get displacement type
     integer              :: acc_type     ! accumulate displacement type
     integer, allocatable :: get_array(:) ! Path counts
     integer, allocatable :: get_idx(:)   ! get indicies
     integer, allocatable :: acc_idx(:)   ! accumulate indices
     integer, allocatable :: acc_array(:) ! unit vector
     integer              :: nlevels      ! # levels in processor tree
     integer              :: mask
     integer              :: localvalue = 0
   contains
     procedure  :: cnt => get_cnt
     procedure  :: del => destroy_sgc
  end type sgc

  interface sgc     ! overload the sgc type with constructor
     procedure constructor
  end interface sgc

contains

  function constructor (comm)
! Global counter constructor
    class(sgc), pointer       :: constructor  ! Global counter
    integer, intent(in)       :: comm      ! communicator
    integer                   :: mask, nlevels, idx, nprocs, err
    integer                   :: tmp_rank, n, level, rank, ws, err1
    integer                   :: disp_unit
    integer(MPI_ADDRESS_KIND) :: size_int, lb, sz, wsz

    allocate (constructor)
! find rank and number of processors:
    write(*,*) ' About to call MPI routines in sgc_mod '
    call MPI_COMM_RANK (comm, rank, err)
    if (err /= 0) call MPI_ABORT (MPI_COMM_WORLD, err, err1)
    constructor%rank = rank
    call MPI_COMM_SIZE (comm, nprocs, err)
    if (err /= 0) call MPI_ABORT (MPI_COMM_WORLD, err, err1)
    constructor%com_size = nprocs

    call MPI_TYPE_GET_EXTENT (MPI_INTEGER, lb, size_int, err)
    if (err /= 0) call MPI_ABORT (MPI_COMM_WORLD, err, err1)
    sz = size_int

    write(*,*) ' Done call to MPI routines in sgc_mod, from task ', rank
! Get the smallest power of two larger than nprocs:
    mask = 1
    nlevels = 0
    do 
       mask = 2 * mask 
       nlevels = nlevels + 1
       if (mask >= nprocs) exit
    end do

    constructor%nlevels = nlevels
    constructor%mask = mask
    disp_unit = sz

! create shared memory window only on processor 0:
    if (rank == 0) then   ! processor holding counter window
       ws = 2 * mask      ! window size (integer units)
       wsz = ws * sz      ! window size in bytes

       allocate (constructor%cntr(ws), errmsg=emsg, stat=ecode)
       if (ecode /= 0) call eprt ('constructor')
! initialize counter memory:
       constructor%cntr = 0
       call MPI_WIN_CREATE (constructor%cntr, wsz, disp_unit, &
            MPI_INFO_NULL, comm, constructor%win, ecode)
       if (ecode /= 0) call eprt ('constructor', 'Win_create error', &
            ecode)
    else  ! window setup on remaining processors:

       ws = 2 * mask      ! window size (integer units)
       wsz = 0            ! window size in bytes
       allocate (constructor%cntr(1), errmsg=emsg, stat=ecode)
       if (ecode /= 0) call eprt ('constructor')
       call MPI_WIN_CREATE (constructor%cntr, wsz, disp_unit, MPI_INFO_NULL, &
            comm, constructor%win, ecode)
       if (ecode /= 0) call eprt ('constructor', 'Win_create error', &
            ecode)
    end if

! allocate index arrays:    
    n = constructor%nlevels
    allocate (constructor%get_array(n), constructor%get_idx(n), &
         constructor%acc_idx(n), constructor%acc_array(n),      &
         errmsg=emsg, stat=ecode)
    if (ecode /= 0) call eprt ('constructor')

! initialize index arrays:
    constructor%get_idx = 0;    constructor%acc_idx = 0
    constructor%get_array = 0;  constructor%acc_array = 1

    mask = mask / 2
    level = 0
    idx   = 0
    tmp_rank = rank
    do while (mask >= 1)
       if (tmp_rank < mask) then
! go to left for acc_idx, go to right for get_idx. 
! set idx=acc_idx for next iteration
          constructor%acc_idx(level+1) = idx + 1
          constructor%get_idx(level+1) = idx + mask*2
          idx                          = idx + 1
       else
! go to right for acc_idx, go to left for get_idx.
! set idx=acc_idx for next iteration
          constructor%acc_idx(level+1) = idx + mask*2
          constructor%get_idx(level+1) = idx + 1
          idx                          = idx + mask*2
       end if
       level = level + 1
       tmp_rank = MOD(tmp_rank, mask)
       mask = mask / 2
    end do


! define get_type:   
    call MPI_Type_create_indexed_block (nlevels, 1, &
         constructor%get_idx, MPI_INTEGER, constructor%get_type, err)
    call MPI_Type_commit (constructor%get_type, err)   ! commit get_type
! define acc_type:
    call MPI_Type_create_indexed_block (nlevels, 1, &
         constructor%acc_idx, MPI_INTEGER, constructor%acc_type, err)
    call MPI_Type_commit (constructor%acc_type, err)   ! commit acc_type

    deallocate (constructor%get_idx, constructor%acc_idx, &
         errmsg=emsg, stat=ecode)
    if (ecode /= 0) call eprt ('constructor')

    write(*,*) ' Done Constructor in sgc_mod, from task ', rank
  end function constructor

  function get_cnt (this)
! Obtain and increment the global iteration count from window on node 0
    integer                   :: get_cnt
    class(sgc), intent(inout) :: this       ! Global counter
    integer                   :: err
    integer(MPI_ADDRESS_KIND) :: offset = 0

    write(*,*) ' In get_cnt in sgc_mod '
!   n = this%nlevels
! lock:
    call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, 0, 0, this%win, err)

    call MPI_Accumulate (this%acc_array, this%nlevels, MPI_INTEGER, 0, &
         offset, 1, this%acc_type, MPI_SUM, this%win, err)
    call MPI_Get (this%get_array, this%nlevels, MPI_INTEGER, 0, offset,&
         1, this%get_type, this%win, err)

! Unlock:
    call MPI_Win_unlock (0, this%win, err)

    get_cnt = this%localvalue + SUM(this%get_array) ! current cnt
    this%localvalue = this%localvalue + 1   ! increment local count

    write(*,*) ' Done get_cnt in sgc_mod '
  end function get_cnt

  subroutine destroy_sgc (this, comm)
! Delete windows used in global counter (destructor function)
    class (sgc), intent(inout) :: this  ! scalable global counter
    integer, intent(in)       :: comm  ! communicator
    integer                   :: err

! Set up barrier to ensure all tasks complete before window cleanup
    call MPI_BARRIER (comm, err)

    call MPI_Win_free (this%win, ecode)
    if (ecode /= 0) call eprt ('destroy_Gctr', 'Win_free', ecode)

! now delete window arrays:
!    if (this%rank == 0) then
       deallocate (this%cntr, errmsg=emsg, stat=ecode)
       if (ecode /= 0) call eprt ('destroy_sgc')
 !   end if
! free indexed array types:
    call MPI_Type_free (this%get_type, err)
    call MPI_Type_free (this%acc_type, err)

    deallocate (this%get_array, this%acc_array, errmsg=emsg, stat=ecode)
    if (ecode /= 0) call eprt ('destroy_sgc')
  end subroutine destroy_sgc
end module sgc_mod

